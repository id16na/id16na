==============
ID16NA project
==============

[![build status](https://gitlab.esrf.fr/ID16NA/id16na/badges/master/build.svg)](http://ID16NA.gitlab-pages.esrf.fr/ID16NA)
[![coverage report](https://gitlab.esrf.fr/ID16NA/id16na/badges/master/coverage.svg)](http://ID16NA.gitlab-pages.esrf.fr/id16na/htmlcov)

ID16NA software & configuration

Latest documentation from master can be found [here](http://ID16NA.gitlab-pages.esrf.fr/id16na)
